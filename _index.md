---
title: français
intertitre: |
  LE FRANÇAIS

type: domain
cascade:
  #colour: sanguine
---

## [mots/](./mots)
## [locutions/](./locutions)
## [l'écoute/](./ecoute)
## [éxposés/](./exposes)
